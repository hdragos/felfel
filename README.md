# README #

This repo provides an implementation to FELFEL Invetory Management task. In Docs folder you can find a Postman v2.1 collection and screeshots demonstrating end to end flows for all endpoints. Enjoy :D

### Framework:###
- .NET Core 3.1 SDK

### How to run?###
1. Open Visual Studio
2. Select Debug | FELFEL.API.Inventory project | Local launch profile

### Further infrastructure and codebase improvements:###
- Restrict CORS to same origin
- Enable framing only for same origin: X-Frame-Options: SAMEORIGIN (prevents clickjacking)
- URL rewrite for WWW subdomain and redirection to HTTPS
- Authentication and Authorization
- Edit build targets in order to copy only appsettings.json and appsettings{environment}.json for a specifc environment
- Code styling, naming, ordering must be adapted to respect project guidelines
- Change InMemory provider with real database. Probably SqlServer. InMemory does not know to handle Expressions
- Expose GUIDs instead of database Id's
- Add test for all business. This contains just example of the main BatchUpdate flow: unit tests for Validators, Controller, Service and one integration test.
- End to end tests: Coved validation attributes, controller, service, but validators are triggers in context of Request/Response. Also, after adding a real DB, you need at least for happy flows to assure every fields ends up in DB and respects constraints.
- DateTimeProvider implementation should be in a domain common project. It can be used by other API's from Inventory microservice. For this exmple, it would be overengineer.

### Nice to have user functionalities:###
- Generate labels with QR Code/Barcode and provide scanning for batches with tablets/phones/barcode readers.
- Analytics layer: Where do we have losses and why? Depending on what information we can collect, we can ingest them in an ELK node or create a prediction AI model for better understanding how stock moves on monthly bases in order to reduce waste.
- Notification system for "LowStock" and "ExpiresSoon" - Email/PushNotification/SMS


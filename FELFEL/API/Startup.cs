using AutoMapper;
using FELFEL.API.Inventory.Middlewares;
using FELFEL.Core.Inventory.Automapper;
using FELFEL.Core.Inventory.Environment;
using FELFEL.Core.Inventory.Interfaces;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Infrastructure.Inventory;
using FELFEL.Infrastructure.Inventory.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NLog;

namespace FELFEL.API.Inventory
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostEnvironment env)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", false)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });

            AddCustomServices(services);
        }

        private void AddCustomServices(IServiceCollection services)
        {
            services.AddAutoMapper(config => config.AddProfile(new AutomapperProfile()));
            services.AddSingleton<ILogger>(LogManager.GetCurrentClassLogger());

            services.AddSingleton<IInventoryContextProvider, InMemoryInventoryContextProvider>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IBatchService, BatchService>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<IDateTimeProvider, DateTimeProvider>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
                app.ConfigureExceptionHandler(logger);
            }

            app.UseRouting();

            app.UseCors();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FELFEL.Core.Inventory.Contracts.Request;
using FELFEL.Core.Inventory.Contracts.Response;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace FELFEL.API.Inventory.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        public ProductController(IProductService productService, IMapper mapper)
        {
            this.productService = productService;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProductResponse>>> GetAllAsync()
        {
            IEnumerable<Product> products = await productService.GetAllAsync();
            var productsResponses = mapper.Map<List<ProductResponse>>(products);

            return Ok(productsResponses);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductResponse>> GetByIdAsync(int id)
        {
            if (!productService.Exists(id))
            {
                return NotFound();
            }

            Product product = await productService.GetByIdAsync(id);
            var productResponse = mapper.Map<ProductResponse>(product);

            return Ok(productResponse);
        }

        [HttpPost]
        public async Task<ActionResult<ProductResponse>> CreateAsync(AddProductRequest addProductRequest)
        {
            Product product = mapper.Map<Product>(addProductRequest);

            await productService.CreateAsync(product);
            var productResponse = mapper.Map<ProductResponse>(product);

            return Created(nameof(CreateAsync), productResponse);
        }

        private readonly IProductService productService;
        private readonly IMapper mapper;
    }
}
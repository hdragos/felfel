﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FELFEL.Core.Inventory.Contracts.Request;
using FELFEL.Core.Inventory.Contracts.Response;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Core.Inventory.Models.Batch;
using Microsoft.AspNetCore.Mvc;

namespace FELFEL.API.Inventory.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BatchController : ControllerBase
    {
        public BatchController(IBatchService batchService, IMapper mapper)
        {
            this.batchService = batchService;
            this.mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<BatchResponse>> CreateAsync(AddBatchRequest addBatchRequest)
        {
            var batch = mapper.Map<Batch>(addBatchRequest);

            await batchService.CreateAsync(batch);
            var batchResponse = mapper.Map<BatchResponse>(batch);

            return Created(nameof(CreateAsync), batchResponse);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BatchResponse>> GetByIdAsync(int id)
        {
            if (!batchService.Exists(id))
            {
                return NotFound();
            }

            Batch batch = await batchService.GetByIdAsync(id);
            var batchResponse = mapper.Map<BatchResponse>(batch);

            return Ok(batchResponse);
        }

        [HttpPut("stock")]
        public async Task<ActionResult> UpdateStockAsync(UpdateBatchStockRequest batchStockRequest)
        {
            if (!batchService.Exists(batchStockRequest.BatchId))
            {
                return NotFound();
            }

            if (!await batchService.IsEnoughStockAsync(batchStockRequest.BatchId, batchStockRequest.UsedUnits))
            {
                return BadRequest($"{nameof(batchStockRequest.UsedUnits)} must be less than actual stock");
            }

            await batchService.UpdateInStockAsync(batchStockRequest.BatchId, batchStockRequest.UsedUnits, batchStockRequest.StockUpdateReason);

            return Ok();
        }

        [HttpGet("freshness")]
        public async Task<ActionResult<List<BatchFreshnessResponse>>> GetFreshnessAsync()
        {
            IEnumerable<BatchFreshnessModel> freshnessModels = await batchService.GetFreshnessAsync();
            List<BatchFreshnessResponse> freshnessResponses = mapper.Map<List<BatchFreshnessResponse>>(freshnessModels);

            return Ok(freshnessResponses);
        }

        [HttpGet("history/{batchId}")]
        public async Task<ActionResult<List<BatchHistoryResponse>>> GetBatchHistoryAsync(int batchId)
        {
            if (!batchService.Exists(batchId))
            {
                return NotFound();
            }

            IEnumerable<BatchHistory> batchHistory = await batchService.GetBatchHistoryAsync(batchId);
            List<BatchHistoryResponse> batchHistoryResponses = mapper.Map<List<BatchHistoryResponse>>(batchHistory);

            return Ok(batchHistoryResponses);
        }

        private readonly IBatchService batchService;
        private readonly IMapper mapper;
    }
}
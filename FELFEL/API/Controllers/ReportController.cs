﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FELFEL.Core.Inventory.Contracts.Response.Report;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Core.Inventory.Models.Report;
using Microsoft.AspNetCore.Mvc;

namespace FELFEL.API.Inventory.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportController : ControllerBase
    {
        public ReportController(IReportService reportService, IProductService productService, IMapper mapper)
        {
            this.reportService = reportService;
            this.productService = productService;
            this.mapper = mapper;
        }

        [HttpGet("batch/{productId}")]
        public async Task<ActionResult<ProductInvetoryResponse>> GetProductInvetoryAsync(int productId)
        {
            if (!productService.Exists(productId))
            {
                return NotFound();
            }

            ProductInventory productInventory = await reportService.GetProductInvetoryAsync(productId);
            var productInvetoryResponse = mapper.Map<ProductInvetoryResponse>(productInventory);

            return Ok(productInvetoryResponse);
        }

        [HttpGet("warehouse")]
        public async Task<ActionResult<List<ProductInvetoryResponse>>> GetWarehouseInvetoryAsync()
        {
            IEnumerable<ProductInventory> warehouseInventory = await reportService.GetWarehouseInventoryAsync();
            var warehouseInvetoryResponse = mapper.Map<List<ProductInvetoryResponse>>(warehouseInventory);

            return Ok(warehouseInvetoryResponse);
        }

        private readonly IReportService reportService;
        private readonly IProductService productService;
        private readonly IMapper mapper;
    }
}
using System.Threading.Tasks;
using AutoMapper;
using FELFEL.API.Inventory.Controllers;
using FELFEL.Core.Inventory.Automapper;
using FELFEL.Core.Inventory.Contracts.Request;
using FELFEL.Core.Inventory.Contracts.Response;
using FELFEL.Core.Inventory.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FELFEL.API.Inventory.UnitTests.Controllers
{
    [TestClass]
    public class BatchControllerTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            batchServiceMock = new Mock<IBatchService>();
            productServiceMock = new Mock<IProductService>();
            controller = new BatchController(batchServiceMock.Object, GetAutomapper());
        }

        [TestMethod]
        public async Task UpdateStockAsync_BatchDoesNotExists_ReturnsNotFound()
        {
            var updateBatchRequest = new UpdateBatchStockRequest { BatchId = 1 };
            batchServiceMock.Setup(bs => bs.Exists(updateBatchRequest.BatchId)).Returns(false);

            ActionResult<BatchResponse> actionResult = await controller.UpdateStockAsync(updateBatchRequest);

            Assert.IsInstanceOfType(actionResult.Result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task UpdateStockAsync_NotEnoughStock_ReturnsBadRequest()
        {
            var updateBatchRequest = new UpdateBatchStockRequest { BatchId = 1, UsedUnits = 10 };
            batchServiceMock.Setup(bs => bs.Exists(updateBatchRequest.BatchId)).Returns(true);
            batchServiceMock.Setup(bs => bs.IsEnoughStockAsync(updateBatchRequest.BatchId, updateBatchRequest.UsedUnits)).ReturnsAsync(false);

            ActionResult<BatchResponse> actionResult = await controller.UpdateStockAsync(updateBatchRequest);

            Assert.IsInstanceOfType(actionResult.Result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public async Task UpdateStockAsync_BatchExistsAndHasInStock_ReturnsBadRequest()
        {
            var updateBatchRequest = new UpdateBatchStockRequest { BatchId = 1, UsedUnits = 10 };
            batchServiceMock.Setup(bs => bs.Exists(updateBatchRequest.BatchId)).Returns(true);
            batchServiceMock.Setup(bs => bs.IsEnoughStockAsync(updateBatchRequest.BatchId, updateBatchRequest.UsedUnits)).ReturnsAsync(true);

            ActionResult<BatchResponse> actionResult = await controller.UpdateStockAsync(updateBatchRequest);

            Assert.IsInstanceOfType(actionResult.Result, typeof(OkResult));
        }

        private IMapper GetAutomapper()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile(new AutomapperProfile()));
            return config.CreateMapper();
        }

        private BatchController controller;
        private Mock<IBatchService> batchServiceMock;
        private Mock<IProductService> productServiceMock;
    }
}
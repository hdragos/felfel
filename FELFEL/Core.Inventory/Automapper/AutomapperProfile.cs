﻿using AutoMapper;
using FELFEL.Core.Inventory.Contracts.Request;
using FELFEL.Core.Inventory.Contracts.Response;
using FELFEL.Core.Inventory.Contracts.Response.Report;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Models.Batch;
using FELFEL.Core.Inventory.Models.Report;

namespace FELFEL.Core.Inventory.Automapper
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateProductMappings();
            CreateBatchMappings();
            CreateReportMappings();
        }

        private void CreateProductMappings()
        {
            CreateMap<AddProductRequest, Product>();
            CreateMap<Product, ProductResponse>();
        }

        private void CreateBatchMappings()
        {
            CreateMap<AddBatchRequest, Batch>();
            CreateMap<Batch, BatchResponse>();
            CreateMap<BatchFreshnessModel, BatchFreshnessResponse>();
            CreateMap<BatchHistory, BatchHistoryResponse>();
        }

        private void CreateReportMappings()
        {
            CreateMap<ProductInventory, ProductInvetoryResponse>();
        }
    }
}
﻿using System;
using FELFEL.Core.Inventory.Interfaces;

namespace FELFEL.Core.Inventory.Environment
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetUtcNow()
        {
            return DateTime.UtcNow;
        }
    }
}
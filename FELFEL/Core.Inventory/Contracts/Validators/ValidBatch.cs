﻿using System;
using System.ComponentModel.DataAnnotations;
using FELFEL.Core.Inventory.Contracts.Request;
using FELFEL.Core.Inventory.Interfaces;
using FELFEL.Core.Inventory.Interfaces.Services;

namespace FELFEL.Core.Inventory.Contracts.Validators
{
    public class ValidBatch : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var productService = (IProductService)validationContext.GetService(typeof(IProductService));
            var dateTimeProvider = (IDateTimeProvider)validationContext.GetService(typeof(IDateTimeProvider));
            var batch = (AddBatchRequest)value;
            if (!productService.Exists(batch.ProductId))
            {
                return new ValidationResult($"Unknown product id: {batch.ProductId}");
            }

            if (IsInvalidExpiringDate(batch.ExpirationDate, dateTimeProvider))
            {
                return new ValidationResult($"Expiration date can't be in the past");
            }

            if (batch.InStock > batch.InitialStock)
            {
                return new ValidationResult($"{nameof(batch.InStock)} can't be greater than {nameof(batch.InitialStock)}");
            }

            return ValidationResult.Success;
        }

        private bool IsInvalidExpiringDate(DateTime dateTime, IDateTimeProvider dateTimeProvider)
        {
            return dateTime.Date < dateTimeProvider.GetUtcNow().Date;
        }
    }
}
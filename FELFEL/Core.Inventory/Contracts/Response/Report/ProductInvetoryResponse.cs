﻿using System.Collections.Generic;

namespace FELFEL.Core.Inventory.Contracts.Response.Report
{
    public class ProductInvetoryResponse
    {
        public string Name { get; set; }
        public ICollection<BatchResponse> Batches { get; set; }
    }
}
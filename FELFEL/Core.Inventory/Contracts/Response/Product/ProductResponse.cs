﻿namespace FELFEL.Core.Inventory.Contracts.Response
{
    public class ProductResponse
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
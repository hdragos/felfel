﻿using System;

namespace FELFEL.Core.Inventory.Contracts.Response
{
    public class BatchResponse
    {
        public int Id { get; set; }
        public string Serial { get; set; }
        public int InStock { get; set; }
        public int InitialStock { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int ProductId { get; set; }
    }
}
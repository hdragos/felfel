﻿using System;
using FELFEL.Core.Inventory.Entities;

namespace FELFEL.Core.Inventory.Contracts.Response
{
    public class BatchHistoryResponse
    {
        public int ID { get; set; }
        public int BatchId { get; set; }
        public int CurrentStock { get; set; }
        public DateTime CreateDate { get; set; }
        public BatchStockUpdateReason UpdateReason { get; set; }
    }
}
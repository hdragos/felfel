﻿using System;
using FELFEL.Core.Inventory.Models;
using FELFEL.Core.Inventory.Models.Batch;

namespace FELFEL.Core.Inventory.Contracts.Response
{
    public class BatchFreshnessResponse
    {
        public string ProductName { get; set; }
        public string BatchSerial { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Freshness Freshness { get; set; }
    }
}
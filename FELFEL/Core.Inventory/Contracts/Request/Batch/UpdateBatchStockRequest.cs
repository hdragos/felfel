﻿using System.ComponentModel.DataAnnotations;
using FELFEL.Core.Inventory.Entities;

namespace FELFEL.Core.Inventory.Contracts.Request
{
    public class UpdateBatchStockRequest
    {
        [Required]
        public int BatchId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int UsedUnits { get; set; }

        [Required]
        public BatchStockUpdateReason StockUpdateReason { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using FELFEL.Core.Inventory.Contracts.Validators;

namespace FELFEL.Core.Inventory.Contracts.Request
{
    [ValidBatch]
    public class AddBatchRequest
    {
        [Required]
        public string Serial { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int InStock { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int InitialStock { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        [Required]
        public int ProductId { get; set; }
    }
}
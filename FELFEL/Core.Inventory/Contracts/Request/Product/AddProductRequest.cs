﻿namespace FELFEL.Core.Inventory.Contracts.Request
{
    public class AddProductRequest
    {
        public string Name { get; set; }
    }
}
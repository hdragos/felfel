﻿namespace FELFEL.Core.Inventory.Models.Batch
{
    public class BatchProductModel
    {
        public string ProductName { get; set; }
        public Entities.Batch Batch { get; set; }
    }
}
﻿namespace FELFEL.Core.Inventory.Models.Batch
{
    public enum Freshness
    {
        Fresh = 0,
        ExpiringToday = 1,
        Expired = 2
    }
}
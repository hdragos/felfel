﻿using System;

namespace FELFEL.Core.Inventory.Models.Batch
{
    public class BatchFreshnessModel
    {
        public string ProductName { get; set; }
        public string BatchSerial { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Freshness Freshness { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace FELFEL.Core.Inventory.Models.Report
{
    public class ProductInventory
    {
        public string Name { get; set; }
        public ICollection<Entities.Batch> Batches { get; set; }
    }
}
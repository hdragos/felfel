﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FELFEL.Core.Inventory.Models.Report;

namespace FELFEL.Core.Inventory.Interfaces.Services
{
    public interface IReportService
    {
        Task<ProductInventory> GetProductInvetoryAsync(int productId);

        Task<IEnumerable<ProductInventory>> GetWarehouseInventoryAsync();
    }
}
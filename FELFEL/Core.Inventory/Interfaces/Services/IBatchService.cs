﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Models.Batch;

namespace FELFEL.Core.Inventory.Interfaces.Services
{
    public interface IBatchService
    {
        Task<Batch> GetByIdAsync(int id);

        bool Exists(int id);

        Task CreateAsync(Batch batch);

        Task UpdateInStockAsync(int id, int usedStock, BatchStockUpdateReason updateReason);

        Task<bool> IsEnoughStockAsync(int id, int usedStock);

        Task<IEnumerable<BatchFreshnessModel>> GetFreshnessAsync();

        Task<IEnumerable<BatchHistory>> GetBatchHistoryAsync(int batchId);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FELFEL.Core.Inventory.Entities;

namespace FELFEL.Core.Inventory.Interfaces.Services
{
    public interface IProductService
    {
        bool Exists(int id);

        Task<IEnumerable<Product>> GetAllAsync();

        Task<Product> GetByIdAsync(int id);

        Task CreateAsync(Product product);
    }
}
﻿using System;

namespace FELFEL.Core.Inventory.Interfaces
{
    public interface IDateTimeProvider
    {
        DateTime GetUtcNow();
    }
}
﻿using System;

namespace FELFEL.Core.Inventory.Entities
{
    public class BatchHistory
    {
        public int ID { get; set; }
        public int BatchId { get; set; }
        public int CurrentStock { get; set; }
        public DateTime CreateDate { get; set; }
        public BatchStockUpdateReason UpdateReason { get; set; }

        public Batch Batch { get; set; }
    }
}
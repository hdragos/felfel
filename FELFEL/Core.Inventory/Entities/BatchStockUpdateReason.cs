﻿namespace FELFEL.Core.Inventory.Entities
{
    public enum BatchStockUpdateReason
    {
        NormalUsage = 0,
        Lost = 1
    }
}
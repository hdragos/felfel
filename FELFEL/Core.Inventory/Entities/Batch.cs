﻿using System;
using System.Collections.Generic;

namespace FELFEL.Core.Inventory.Entities
{
    public class Batch
    {
        public int ID { get; set; }
        public string Serial { get; set; }
        public int InStock { get; set; }
        public int InitialStock { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int ProductId { get; set; }

        public Product Product { get; set; }
        public ICollection<BatchHistory> BatchHistory { get; set; }
    }
}
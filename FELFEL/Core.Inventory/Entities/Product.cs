﻿using System.Collections.Generic;

namespace FELFEL.Core.Inventory.Entities
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public ICollection<Batch> Batches { get; set; }
    }
}
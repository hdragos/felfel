using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Environment;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Infrastructure.Inventory.Services;
using FELFEL.Persistence.Inventory;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FELFEL.Infrastructure.Inventory.UnitTests.Services
{
    [TestClass]
    public class BatchServiceTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var contextProvider = new InMemoryInventoryContextProvider();
            context = contextProvider.Get();
            batchService = new BatchService(contextProvider, new DateTimeProvider());
            productService = new ProductService(contextProvider);
        }

        [TestMethod]
        public async Task Create_ExpirationDateContainsTime_AddsWithZeroTime()
        {
            var expirationDate = new DateTime(2020, 10, 11, 10, 11, 12);
            var batch = new Batch
            {
                ExpirationDate = expirationDate
            };

            await batchService.CreateAsync(batch);

            Assert.AreEqual(1, context.Batches.Count());
            Assert.AreEqual(expirationDate.Date, batch.ExpirationDate);
        }

        [TestMethod]
        [DataRow(10, new[] { 5, 1, 2 })]
        [DataRow(10, new[] { 10 })]
        [DataRow(10, new[] { 11, 15 })]
        public async Task UpdateInStock_MultipleStockUpdates_SubstractsUsedStock(int inStock, int[] usedStocks)
        {
            var product = new Product
            {
                Name = "Pasta",
                Batches = new List<Batch>
                {
                    new Batch
                    {
                        InStock = inStock
                    }
                }
            };
            await productService.CreateAsync(product);

            foreach (int usedStock in usedStocks)
            {
                await batchService.UpdateInStockAsync(product.ID, usedStock, BatchStockUpdateReason.NormalUsage);
            }

            Assert.AreEqual(inStock - usedStocks.Sum(), context.Batches.First().InStock);
        }

        [TestMethod]
        [DataRow(10, new[] { 5, 1, 2 }, BatchStockUpdateReason.NormalUsage)]
        [DataRow(10, new[] { 10 }, BatchStockUpdateReason.Lost)]
        [DataRow(10, new[] { 11, 15 }, BatchStockUpdateReason.NormalUsage)]
        public async Task UpdateInStock_MultipleStockUpdates_AddsBatchHistoryForEach(int inStock, int[] usedStocks, BatchStockUpdateReason reason)
        {
            var product = new Product
            {
                Name = "Pasta",
                Batches = new List<Batch>
                {
                    new Batch
                    {
                        InStock = inStock
                    }
                }
            };
            await productService.CreateAsync(product);

            for (int i = 0; i < usedStocks.Length; i++)
            {
                await batchService.UpdateInStockAsync(product.ID, usedStocks[i], reason);

                BatchHistory batchHistory = context.BatchHistory.OrderByDescending(bh => bh.CreateDate).First();
                Batch batch = context.Batches.First();
                Assert.AreEqual(batch.ID, batchHistory.BatchId);
                Assert.AreEqual(batch.InStock, batchHistory.CurrentStock);
                Assert.AreEqual(reason, batchHistory.UpdateReason);
            }
        }

        private IBatchService batchService;
        private IProductService productService;
        private InventoryContext context;
    }
}
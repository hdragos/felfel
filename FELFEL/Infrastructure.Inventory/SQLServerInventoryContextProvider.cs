﻿using FELFEL.Persistence.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FELFEL.Infrastructure.Inventory
{
    public class SQLServerInventoryContextProvider : IInventoryContextProvider
    {
        public SQLServerInventoryContextProvider(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public InventoryContext Get()
        {
            var optionsBuilder = new DbContextOptionsBuilder<InventoryContext>();
            DbContextOptions<InventoryContext> options = optionsBuilder
                .UseSqlServer(configuration.GetConnectionString(InventoryConnection))
                .Options;

            return new InventoryContext(options);
        }

        private const string InventoryConnection = "InventoryConnection";
        private readonly IConfiguration configuration;
    }
}
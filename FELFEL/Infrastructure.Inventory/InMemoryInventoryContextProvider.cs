﻿using FELFEL.Persistence.Inventory;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace FELFEL.Infrastructure.Inventory
{
    public class InMemoryInventoryContextProvider : IInventoryContextProvider
    {
        public InventoryContext Get()
        {
            if (instance == null)
            {
                var contextOptions = new DbContextOptionsBuilder<InventoryContext>()
                    .UseInMemoryDatabase("FELFEL", new InMemoryDatabaseRoot())
                    .Options;

                instance = new InventoryContext(contextOptions);
            }

            return instance;
        }

        private InventoryContext instance;
    }
}
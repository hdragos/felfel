﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Core.Inventory.Models.Report;
using FELFEL.Persistence.Inventory;
using Microsoft.EntityFrameworkCore;

namespace FELFEL.Infrastructure.Inventory.Services
{
    public class ReportService : IReportService
    {
        public ReportService(IInventoryContextProvider contextProvider)
        {
            this.contextProvider = contextProvider;
            context = contextProvider.Get();
        }

        public async Task<ProductInventory> GetProductInvetoryAsync(int productId)
        {
            Product product = await context.Products.FirstAsync(p => p.ID == productId);
            var batches = await context.Batches.Where(batch => batch.ProductId == productId).ToListAsync();

            return new ProductInventory
            {
                Name = product.Name,
                Batches = batches
            };
        }

        public async Task<IEnumerable<ProductInventory>> GetWarehouseInventoryAsync()
        {
            return await context.Products.Select(product => GetProductInvetoryAsync(product.ID).GetAwaiter().GetResult()).ToListAsync();
        }

        private readonly IInventoryContextProvider contextProvider;
        private readonly InventoryContext context;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Interfaces;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Core.Inventory.Models.Batch;
using FELFEL.Persistence.Inventory;
using Microsoft.EntityFrameworkCore;

namespace FELFEL.Infrastructure.Inventory.Services
{
    public class BatchService : IBatchService
    {
        public BatchService(IInventoryContextProvider contextProvider, IDateTimeProvider dateTimeProvider)
        {
            this.dateTimeProvider = dateTimeProvider;
            context = contextProvider.Get();
        }

        public Task<Batch> GetByIdAsync(int id)
        {
            return context.Batches.FirstAsync(batch => batch.ID == id);
        }

        public bool Exists(int id)
        {
            return context.Batches.Any(batch => batch.ID == id);
        }

        public async Task CreateAsync(Batch batch)
        {
            batch.ExpirationDate = batch.ExpirationDate.Date;

            context.Add(batch);
            await context.SaveChangesAsync();
        }

        public async Task UpdateInStockAsync(int id, int usedStock, BatchStockUpdateReason updateReason)
        {
            Batch batch = await GetByIdAsync(id);
            batch.InStock -= usedStock;
            var batchHistory = new BatchHistory
            {
                BatchId = batch.ID,
                CreateDate = dateTimeProvider.GetUtcNow(),
                CurrentStock = batch.InStock,
                UpdateReason = updateReason
            };

            context.Update(batch);
            context.Add(batchHistory);
            await context.SaveChangesAsync();
        }

        public async Task<bool> IsEnoughStockAsync(int id, int usedStock)
        {
            Batch batch = await GetByIdAsync(id);

            return batch.InStock >= usedStock;
        }

        public async Task<IEnumerable<BatchFreshnessModel>> GetFreshnessAsync()
        {
            var utcNow = dateTimeProvider.GetUtcNow();
            bool Expired(DateTime expirationDate) => utcNow.Date > expirationDate.Date;
            bool ExpiringToday(DateTime expirationDate) => utcNow.Date == expirationDate.Date;
            bool Fresh(DateTime expirationDate) => utcNow.Date < expirationDate.Date;

            List<IGrouping<DateTime, BatchProductModel>> groupedBatches = await GroupedBatchesAsync();

            IEnumerable<BatchFreshnessModel> expired = GetBatchesFresshness(groupedBatches, Expired, Freshness.Expired);
            IEnumerable<BatchFreshnessModel> expiringToday = GetBatchesFresshness(groupedBatches, ExpiringToday, Freshness.ExpiringToday);
            IEnumerable<BatchFreshnessModel> fresh = GetBatchesFresshness(groupedBatches, Fresh, Freshness.Fresh);

            return expired.Concat(expiringToday).Concat(fresh);
        }

        public async Task<IEnumerable<BatchHistory>> GetBatchHistoryAsync(int batchId)
        {
            return await context.BatchHistory.Where(batchHistory => batchHistory.BatchId == batchId)
                .OrderBy(batchHitory => batchHitory.CreateDate)
                .ToListAsync();
        }

        private async Task<List<IGrouping<DateTime, BatchProductModel>>> GroupedBatchesAsync()
        {
            List<BatchProductModel> batchProducts = await context.Batches
                .Include(batch => batch.Product)
                .Select(batch => new BatchProductModel
                {
                    ProductName = batch.Product.Name,
                    Batch = batch
                }).ToListAsync();

            IEnumerable<IGrouping<DateTime, BatchProductModel>> grouping = batchProducts.GroupBy(batchProduct => batchProduct.Batch.ExpirationDate);

            return grouping.ToList();
        }

        // TODO this works only with SQL Server because InMemory does not know how to translate provided expressions
        // Provided InMemory implementation which is not optimal for large datasets as LINQ methods from below

        //private async Task<List<IGrouping<DateTime, BatchProductModel>>> GroupedBatchesAsync()
        //{
        //    return await context.Batches.Join(context.Products, batch => batch.ProductId, product => product.ID,
        //            (batch, product) => new BatchProductModel
        //            {
        //                Batch = batch,
        //                ProductName = product.Name
        //            })
        //        .GroupBy(batchProduct => batchProduct.Batch.ExpirationDate)
        //        .ToListAsync();
        //}

        private IEnumerable<BatchFreshnessModel> GetBatchesFresshness(List<IGrouping<DateTime, BatchProductModel>> groupedBatches, Func<DateTime, bool> expiration, Freshness freshness)
        {
            IEnumerable<IGrouping<DateTime, BatchProductModel>> filteredBatchProducts = groupedBatches.Where(group => expiration(group.Key));

            var freshnessModels = new List<BatchFreshnessModel>();
            foreach (IGrouping<DateTime, BatchProductModel> batchProducts in filteredBatchProducts)
            {
                foreach (BatchProductModel batchProduct in batchProducts)
                {
                    freshnessModels.Add(new BatchFreshnessModel
                    {
                        Freshness = freshness,
                        ProductName = batchProduct.ProductName,
                        BatchSerial = batchProduct.Batch.Serial,
                        ExpirationDate = batchProduct.Batch.ExpirationDate
                    });
                }
            }

            return freshnessModels;
        }

        private readonly InventoryContext context;
        private readonly IDateTimeProvider dateTimeProvider;
    }
}
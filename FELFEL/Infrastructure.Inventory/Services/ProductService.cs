﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Persistence.Inventory;
using Microsoft.EntityFrameworkCore;

namespace FELFEL.Infrastructure.Inventory.Services
{
    public class ProductService : IProductService
    {
        public ProductService(IInventoryContextProvider contextProvider)
        {
            context = contextProvider.Get();
        }

        public bool Exists(int id)
        {
            return context.Products.Any(product => product.ID == id);
        }

        public async Task<IEnumerable<Product>> GetAllAsync()
        {
            return await context.Products.ToListAsync();
        }

        public async Task<Product> GetByIdAsync(int id)
        {
            return await context.Products.FirstAsync(product => product.ID == id);
        }

        public async Task CreateAsync(Product product)
        {
            context.Add(product);
            await context.SaveChangesAsync();
        }

        private readonly InventoryContext context;
    }
}
﻿using FELFEL.Persistence.Inventory;

namespace FELFEL.Infrastructure.Inventory
{
    public interface IInventoryContextProvider
    {
        InventoryContext Get();
    }
}
﻿using FELFEL.Core.Inventory.Entities;
using FELFEL.Persistence.Inventory.Configurations;
using Microsoft.EntityFrameworkCore;

namespace FELFEL.Persistence.Inventory
{
    public class InventoryContext : DbContext
    {
        public InventoryContext(DbContextOptions<InventoryContext> options) : base(options)
        {
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Batch> Batches { get; set; }
        public virtual DbSet<BatchHistory> BatchHistory { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new BatchConfiguration());
        }
    }
}
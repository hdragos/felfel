﻿using FELFEL.Core.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FELFEL.Persistence.Inventory.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasMany(product => product.Batches)
                .WithOne(batch => batch.Product)
                .HasForeignKey(batch => batch.ProductId);
        }
    }
}
﻿using FELFEL.Core.Inventory.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FELFEL.Persistence.Inventory.Configurations
{
    public class BatchConfiguration : IEntityTypeConfiguration<Batch>
    {
        public void Configure(EntityTypeBuilder<Batch> builder)
        {
            builder.HasMany(batch => batch.BatchHistory)
                .WithOne(batchHistory => batchHistory.Batch)
                .HasForeignKey(batchHistory => batchHistory.BatchId);
        }
    }
}
using System;
using System.ComponentModel.DataAnnotations;
using FELFEL.Core.Inventory.Contracts.Request;
using FELFEL.Core.Inventory.Contracts.Validators;
using FELFEL.Core.Inventory.Interfaces;
using FELFEL.Core.Inventory.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FELFEL.Core.Inventory.UnitTests.Validators
{
    [TestClass]
    public class ValidBatchTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            mockProductService = new Mock<IProductService>();
            mockDateTimeProvider = new Mock<IDateTimeProvider>();
            validBatch = new ValidBatch();
        }

        [TestMethod]
        public void IsValid_ProductIdDoesNotExist_ReturnsError()
        {
            var addBatchRequest = new AddBatchRequest { ProductId = 1 };
            mockProductService.Setup(ps => ps.Exists(addBatchRequest.ProductId)).Returns(false);
            var collection = new ServiceCollection();
            collection.AddScoped(typeof(IProductService), _ => mockProductService.Object);
            var validationContext = new ValidationContext(addBatchRequest, collection.BuildServiceProvider(), null);

            Assert.ThrowsException<ValidationException>(() => validBatch.Validate(addBatchRequest, validationContext));
        }

        [TestMethod]
        [DateTimeDataSource]
        public void IsValid_ExpirationDateIsLessThanToday_ReturnsError(DateTime expirationDate)
        {
            var addBatchRequest = new AddBatchRequest { ProductId = 1, ExpirationDate = expirationDate };
            mockProductService.Setup(ps => ps.Exists(addBatchRequest.ProductId)).Returns(true);
            mockDateTimeProvider.Setup(dt => dt.GetUtcNow()).Returns(new DateTime(2020, 10, 30));
            var collection = new ServiceCollection();
            collection.AddScoped(typeof(IProductService), _ => mockProductService.Object);
            collection.AddScoped(typeof(IDateTimeProvider), _ => mockDateTimeProvider.Object);
            var validationContext = new ValidationContext(addBatchRequest, collection.BuildServiceProvider(), null);

            Assert.ThrowsException<ValidationException>(() => validBatch.Validate(addBatchRequest, validationContext));
        }

        [TestMethod]
        [DateTimeDataSource]
        public void IsValid_ExpirationIsGreaterOrEqualThanToday_ReturnsValid(DateTime expirationDate)
        {
            var addBatchRequest = new AddBatchRequest { ProductId = 1, ExpirationDate = expirationDate };
            mockProductService.Setup(ps => ps.Exists(addBatchRequest.ProductId)).Returns(true);
            mockDateTimeProvider.Setup(dt => dt.GetUtcNow()).Returns(new DateTime(2020, 10, 11));
            var collection = new ServiceCollection();
            collection.AddScoped(typeof(IProductService), _ => mockProductService.Object);
            collection.AddScoped(typeof(IDateTimeProvider), _ => mockDateTimeProvider.Object);
            var validationContext = new ValidationContext(addBatchRequest, collection.BuildServiceProvider(), null);

            validBatch.Validate(addBatchRequest, validationContext);

            Assert.IsNull(validBatch.ErrorMessage);
        }

        private Mock<IProductService> mockProductService;
        private Mock<IDateTimeProvider> mockDateTimeProvider;
        private ValidBatch validBatch;
    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace FELFEL.Core.Inventory.UnitTests.Validators
{
    [AttributeUsage(AttributeTargets.Method)]
    public class DateTimeDataSource : Attribute, ITestDataSource
    {
        public IEnumerable<object[]> GetData(MethodInfo methodInfo)
        {
            var dateTimes = new List<object[]>
            {
                new[] { (object)new DateTime(2020, 10, 11, 12, 13, 14)  },
                new[] { (object)new DateTime(2020, 10, 11, 00, 00, 00)  }
            };

            return dateTimes;
        }

        public string GetDisplayName(MethodInfo methodInfo, object[] data)
        {
            return string.Empty;
        }
    }
}
﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FELFEL.API.Inventory.Controllers;
using FELFEL.Core.Inventory.Automapper;
using FELFEL.Core.Inventory.Contracts.Request;
using FELFEL.Core.Inventory.Contracts.Response;
using FELFEL.Core.Inventory.Entities;
using FELFEL.Core.Inventory.Interfaces;
using FELFEL.Core.Inventory.Interfaces.Services;
using FELFEL.Infrastructure.Inventory;
using FELFEL.Infrastructure.Inventory.Services;
using FELFEL.Persistence.Inventory;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FELFEL.API.Inventory.IntegrationTests
{
    [TestClass]
    public class BatchTests
    {
        [TestInitialize]
        public void TestInitialize()
        {
            var contextProvider = new InMemoryInventoryContextProvider();
            context = contextProvider.Get();
            dateTimeProviderMock = new Mock<IDateTimeProvider>();
            batchService = new BatchService(contextProvider, dateTimeProviderMock.Object);
            productService = new ProductService(contextProvider);
            mapper = GetAutomapper();

            batchController = new BatchController(batchService, mapper);
            productController = new ProductController(productService, mapper);
        }

        [TestMethod]
        public async Task UpdateBatchMultipleTimes_UpdatesBatchAndItsHistory()
        {
            await CreateProductAsync("Potato");
            var pasta = await CreateProductAsync("Pasta");

            Assert.AreEqual(2, context.Products.Count());
            Assert.AreEqual(pasta.Name, context.Products.First(p => p.ID == pasta.ID).Name);

            await CreateBatchAsync(pasta.ID);
            var pastaBatch = await CreateBatchAsync(pasta.ID);

            Assert.AreEqual(2, context.Batches.Count(b => b.ProductId == pastaBatch.Id));

            int usedUnits = 10;
            int previuousStock = pastaBatch.InStock;
            dateTimeProviderMock.Setup(dt => dt.GetUtcNow()).Returns(DateTime.UtcNow);
            await UpdateStockAsync(pastaBatch.Id, usedUnits, BatchStockUpdateReason.NormalUsage);
            pastaBatch = await GetBatchAsync(pastaBatch.Id);
            var batchHistory = context.BatchHistory.First(bh => bh.CreateDate.Date == dateTimeProviderMock.Object.GetUtcNow().Date);

            Assert.AreEqual(previuousStock - usedUnits, pastaBatch.InStock);
            Assert.AreEqual(1, context.BatchHistory.Count());
            Assert.AreEqual(pastaBatch.Id, batchHistory.BatchId);
            Assert.AreEqual(pastaBatch.InStock, batchHistory.CurrentStock);
            Assert.AreEqual(BatchStockUpdateReason.NormalUsage, batchHistory.UpdateReason);

            usedUnits = 5;
            previuousStock = pastaBatch.InStock;
            dateTimeProviderMock.Setup(dt => dt.GetUtcNow()).Returns(DateTime.UtcNow.AddDays(1));
            await UpdateStockAsync(pastaBatch.Id, usedUnits, BatchStockUpdateReason.Lost);
            pastaBatch = await GetBatchAsync(pastaBatch.Id);
            batchHistory = context.BatchHistory.First(bh => bh.CreateDate.Date == dateTimeProviderMock.Object.GetUtcNow().Date);

            Assert.AreEqual(previuousStock - usedUnits, pastaBatch.InStock);
            Assert.AreEqual(2, context.BatchHistory.Count());
            Assert.AreEqual(pastaBatch.Id, batchHistory.BatchId);
            Assert.AreEqual(pastaBatch.InStock, batchHistory.CurrentStock);
            Assert.AreEqual(BatchStockUpdateReason.Lost, batchHistory.UpdateReason);
        }

        private async Task<BatchResponse> GetBatchAsync(int batchId)
        {
            var actionResult = await batchController.GetByIdAsync(batchId);
            return (BatchResponse)((OkObjectResult)actionResult.Result).Value;
        }

        private async Task<ProductResponse> CreateProductAsync(string productName)
        {
            ActionResult<ProductResponse> actionResult = await productController.CreateAsync(new AddProductRequest { Name = productName });
            var product = (ProductResponse)((CreatedResult)actionResult.Result).Value;

            return product;
        }

        private async Task<BatchResponse> CreateBatchAsync(int productId)
        {
            dateTimeProviderMock.Setup(dt => dt.GetUtcNow()).Returns(DateTime.UtcNow);
            ActionResult<BatchResponse> batchResult = await batchController.CreateAsync(new AddBatchRequest
            {
                ProductId = productId,
                ExpirationDate = dateTimeProviderMock.Object.GetUtcNow().AddDays(10),
                InitialStock = 100,
                InStock = 100,
                Serial = "1235"
            });

            return (BatchResponse)((CreatedResult)batchResult.Result).Value;
        }

        private async Task UpdateStockAsync(int batchId, int usedUnits, BatchStockUpdateReason reason)
        {
            await batchController.UpdateStockAsync(new UpdateBatchStockRequest
            {
                BatchId = batchId,
                UsedUnits = usedUnits,
                StockUpdateReason = reason
            });
        }

        private IMapper GetAutomapper()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile(new AutomapperProfile()));
            return config.CreateMapper();
        }

        private IBatchService batchService;
        private IProductService productService;
        private Mock<IDateTimeProvider> dateTimeProviderMock;
        private BatchController batchController;
        private ProductController productController;
        private InventoryContext context;
        private IMapper mapper;
    }
}